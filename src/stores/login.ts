import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const loginName = ref("");
  const email = ref("@gmail.com");
  const messageStore = useMessageStore();
  const userStore = useUserStore();
  const isLogin = computed(() => {
    return loginName.value !== ""; //name not empty
  });
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
      email.value = loginName.value + email.value;
    } else {
      messageStore.showMessage("Login or password is incorrect.");
    }
  };

  const logout = (): void => {
    loginName.value = "";
    localStorage.removeItem("loginName");
    email.value = "@gmail.com";
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
    email.value = loginName.value + email.value;
  };

  return { loginName, isLogin, login, logout, loadData, email };
});
